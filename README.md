# University Management System

## Overview
This application is a part of a Domain-Driven Design (DDD) exercise, demonstrating the implementation of DDD principles using EF Core for managing Student and Course aggregates. The application includes tests for the StudentRepository and provides a task for candidates to implement similar functionality for the CourseRepository, including Fluent API configurations.

## Running the Application
Before running the application, ensure you have the following prerequisites:

- .NET Core SDK installed.
- Access to a SQL Server instance (LocalDB or other).

## Steps to Run:
1. In UniversityDbContext, replace the connection string with your actual database connection string.

2. Run the migration to create your database schema. Otherwise, ensure your database schema matches the model definitions.

3. Build the application and run it. The Program.cs file contains test scripts that will execute upon running the application.

## Candidate Task
## Overview
Your task is to extend the existing application by implementing the CourseRepository and configuring the Fluent API for the Course aggregate, using the StudentRepository and its Fluent API configuration as a guide.

## Steps for the Candidate:
1. Review Existing Implementations:
 - Understand the StudentRepository and how it manages the persistence of Student aggregates, including enrollments and grades.
 - Examine the Fluent API configuration in UniversityDbContext for the Student aggregate.
2. Implement the CourseRepository:

 - Create a CourseRepository class that handles CRUD operations for Course aggregates and manages Assignment entities within a course.
3. Configure Fluent API for Course Aggregate:
 - In UniversityDbContext, implement the Fluent API configuration for the Course aggregate, ensuring correct mapping of relationships and properties.

4. Implement Test code in Program.cs:
 - Uncomment or add tests in Program.cs for the CourseRepository, demonstrating adding a course, managing assignments, and linking courses to students.

5. Ensure Correctness and Integration:
 - Run the application to ensure your implementation correctly handles courses and assignments and integrates properly with the existing student management functionality.

6. Document Your Work:

 - Include comments explaining your design choices and how they align with DDD principles, particularly in your Fluent API configuration.

## Notes for the Candidate
 - Focus on maintaining a clean separation of concerns and ensuring that the domain logic is encapsulated within the domain models.
 - Pay attention to how the application handles relationships between entities in the Fluent API configuration and apply similar principles in your implementation.
 - Handle any exceptions and edge cases that might occur during database operations.
 - Be thorough in your testing to ensure that both the repository and Fluent API configurations are working as expected.

Good luck with your implementation!
