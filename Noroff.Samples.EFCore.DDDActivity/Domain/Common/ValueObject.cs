﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Noroff.Samples.EFCore.DDDActivity.Domain.Common
{
    /// <summary>
    /// Value objects are objects that describe some characteristic or attribute but carry no concept of identity.
    /// </summary>
    public abstract class ValueObject
    {
        // Override equality logic
        public override bool Equals(object obj)
        {
            if (obj == null || obj.GetType() != GetType())
                return false;

            return GetEqualityComponents().SequenceEqual(
                ((ValueObject)obj).GetEqualityComponents());
        }

        // Method to get the components for equality check
        protected abstract IEnumerable<object> GetEqualityComponents();

        // Override GetHashCode
        public override int GetHashCode()
        {
            return GetEqualityComponents()
                .Aggregate(1, (current, obj) => current * 23 + (obj?.GetHashCode() ?? 0));
        }

        // Other value object logic can be added here
    }

}
