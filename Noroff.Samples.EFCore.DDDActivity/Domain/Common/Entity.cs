﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Noroff.Samples.EFCore.DDDActivity.Domain.Common
{
    /// <summary>
    /// Entities are objects that are defined not just by their attributes, but by a thread of continuity and identity.
    /// </summary>
    public abstract class Entity
    {
        public int Id { get; protected set; }

        // Override equality logic
        public override bool Equals(object obj)
        {
            if (obj == null || obj.GetType() != GetType())
                return false;

            var other = (Entity)obj;
            return Id == other.Id;
        }

        // Override GetHashCode
        public override int GetHashCode()
        {
            return Id.GetHashCode();
        }

        // Other common entity logic can be added here
    }

}
