﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Noroff.Samples.EFCore.DDDActivity.Domain.Common
{
    /// <summary>
    /// An AggregateRoot is a special kind of entity. 
    /// All external access to the aggregate goes through the aggregate root to ensure the integrity of the aggregate.
    /// </summary>
    public abstract class AggregateRoot : Entity
    {
        // AggregateRoot specific logic can be added here
        // Currently, it just inherits from Entity. If there are aggregate-specific
        // behaviors, they would be added here.
        // This just makes it easier when designing repositories as they are linked to AggregateRoots
    }
}
