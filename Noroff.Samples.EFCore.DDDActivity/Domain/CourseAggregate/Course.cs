﻿using Noroff.Samples.EFCore.DDDActivity.Domain.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Noroff.Samples.EFCore.DDDActivity.Domain.CourseAggregate
{
    public class Course : AggregateRoot
    {
        public string Title { get; private set; }

        // Navigation properties
        public ICollection<Assignment> Assignments { get; private set; }

        // Constructor for creating a new course
        public Course(string title)
        {
            // Business rule: Validate title
            Title = title;
            Assignments = new List<Assignment>();
        }

        // Methods for managing assignments
        public void AddAssignment(string title, string description, DateTime dueDate)
        {
            // Business rule: Validate assignment details, due dates, etc.
            // Done in the Assignment constructor.
            Assignments.Add(new Assignment(title,description,dueDate));
        }

        public void UpdateExistingAssignment(int assignmentId, string newTitle, string newDescription, DateTime newDueDate)
        {
            var existingAssignemnt = Assignments.First(a => a.Id == assignmentId);
            if (existingAssignemnt != null)
            {
                // Validation would be done in the assignment class
                existingAssignemnt.UpdateAssignment(newTitle, newDescription, newDueDate);
            }
        }
    }
}
