﻿using Noroff.Samples.EFCore.DDDActivity.Domain.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Noroff.Samples.EFCore.DDDActivity.Domain.CourseAggregate
{
    public class Assignment : Entity
    {
        public string Title { get; private set; }
        public string Description { get; private set; }
        public int CourseId { get; private set; }
        public DateTime DueDate { get; private set; }

        // Constructor for creating a new assignment
        public Assignment(string title, string description, DateTime dueDate)
        {
            // Business rule: Validate assignment details and due date
            Title = title;
            Description = description;
            DueDate = dueDate;
        }

        // Method to update assignment details
        public void UpdateAssignment(string newTitle, string newDescription, DateTime newDueDate)
        {
            // Business rule: Validate new details and due date
            Title = newTitle;
            Description = newDescription;
            DueDate = newDueDate;
        }
    }

}
