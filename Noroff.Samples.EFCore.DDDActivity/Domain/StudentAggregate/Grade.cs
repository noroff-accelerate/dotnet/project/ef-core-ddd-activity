﻿using Noroff.Samples.EFCore.DDDActivity.Domain.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Noroff.Samples.EFCore.DDDActivity.Domain.StudentAggregate
{
    public class Grade : ValueObject
    {
        public string Letter { get; private set; }
        public decimal NumericValue { get; private set; }

        private Grade(string letter, decimal numericValue)
        {
            Letter = letter;
            NumericValue = numericValue;
        }

        /// <summary>
        /// The reason we have a private constructor and expose a static factory method is that we have the opportunity to perform validation and throw and exception.
        /// <para></para>
        /// We do not actually do it in this example, but this is where you could validate that a given combination of letter and number make sense (i.e a C isnt listed as 95%).
        /// <para></para>
        /// It is common for any object with validation to have these static factory methods, it even may extend to objects that do not for the sake of consistency.
        /// </summary>
        /// <param name="letter"></param>
        /// <param name="numericValue"></param>
        /// <returns></returns>
        public static Grade Create(string letter, decimal numericValue)
        {
            // Business rule: Validate grade values
            return new Grade(letter, numericValue);
        }

        protected override IEnumerable<object> GetEqualityComponents()
        {
            // Each yield return statement returns a component
            // that should be involved in the equality check
            yield return Letter;
            yield return NumericValue;
        }
    }

}
