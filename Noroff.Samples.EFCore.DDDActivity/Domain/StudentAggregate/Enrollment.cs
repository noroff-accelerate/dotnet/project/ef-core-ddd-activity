﻿using Noroff.Samples.EFCore.DDDActivity.Domain.Common;
using System;
using System.Collections.Generic;
using System.Diagnostics.Metrics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Noroff.Samples.EFCore.DDDActivity.Domain.StudentAggregate
{
    public class Enrollment : Entity
    {
        public int CourseId { get; private set; }
        public int StudentId { get; private set; }

        // Grade as value object
        public Grade? Grade { get; private set; }

        // Constructor for creating a new enrollment
        public Enrollment(int courseId)
        {
            CourseId = courseId;
            Grade = Grade.Create("N/A", 0);
        }

        // Method to update grade
        public void UpdateGrade(string letter, decimal numericValue)
        {
            // Business rule: Validate grade change, check for grade improvement policy, etc.
            // Done in the create method in grade.
            // We dont keep a history, we just replace, for simplicity.
            Grade = Grade.Create(letter, numericValue);
        }
    }

}
