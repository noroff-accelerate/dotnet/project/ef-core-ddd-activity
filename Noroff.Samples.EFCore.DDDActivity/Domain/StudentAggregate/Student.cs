﻿using Microsoft.EntityFrameworkCore;
using Microsoft.VisualBasic;
using Noroff.Samples.EFCore.DDDActivity.Domain.Common;
using Noroff.Samples.EFCore.DDDActivity.Domain.CourseAggregate;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Drawing;
using System.Linq;
using System.Reflection.Metadata;
using System.Text;
using System.Threading.Channels;
using System.Threading.Tasks;
using static Microsoft.EntityFrameworkCore.DbLoggerCategory;
using static System.Net.Mime.MediaTypeNames;

namespace Noroff.Samples.EFCore.DDDActivity.Domain.StudentAggregate
{
    /// <summary>
    /// Aggregate root for the StudentAggregate. 
    /// All changes to related entities go through here.
    /// Its the job of the aggregateroot to ensure all its entities are valid at all times.
    /// </summary>
    public class Student : AggregateRoot
    {
        public string Name { get; private set; }
        public string Email { get; private set; }
        public string Address { get; private set; }
        public DateTime DateOfBirth { get; private set; }
        // 
        /// <summary>
        /// Concurrency token for optimistic locking. <para></para>
        /// How It Works
        /// RowVersion Property: The RowVersion property is marked with the [Timestamp] attribute, indicating to EF Core that this property should be treated as a concurrency token. EF Core will automatically generate and update this value every time a Student entity is updated. 
        /// <para></para>
        /// Concurrency Check During Save: When you attempt to save changes to a Student entity, EF Core checks the RowVersion value against the value in the database. If the values differ, it means the entity has been modified by another process since it was loaded, and EF Core will throw a DbUpdateConcurrencyException.
        /// <para></para>
        /// Handling Concurrency Conflicts: In your application, you should handle DbUpdateConcurrencyException to implement your strategy for dealing with concurrency conflicts. This could involve retrying the operation, prompting the user to reload the entity and reapply their changes, or other domain-specific resolution strategies.
        /// <para></para>
        /// By adding this optimistic locking mechanism, you help ensure that concurrent updates to a Student entity are handled safely, preventing conflicts and data loss in a multi-user environment. This is particularly important in systems where entities can be concurrently accessed and modified by different parts of the application or by different users.
        /// </summary>
        [Timestamp]
        public byte[] RowVersion { get; private set; }

        // Navigation properties
        public ICollection<Enrollment> Enrollments { get; private set; }

        // Constructor for creating a new student
        public Student(string name, string email, string address, DateTime dateOfBirth)
        {
            // Business rule: Validate name and email
            Name = name;
            Email = email;
            Enrollments = new List<Enrollment>();
            Address = address;
            DateOfBirth = dateOfBirth;
        }

        /// <summary>
        /// Method to add enrollment.
        /// <para></para>
        /// The validation of course being a valid courseId is done somewhere else, as it does not belong here.
        /// <para></para>
        /// This can be achieved in either a domain service (CourseValidationService) or a repository where dbContext is queried with courseId.
        /// <para></para>
        /// The main idea is that we dont want to overly couple our domain to our data access.
        /// </summary>
        /// <param name="course"></param>
        public void Enroll(int course)
        {
            // Business rule: Check for duplicate enrollment, validate course capacity, etc.
            Enrollments.Add(new Enrollment(courseId: course));
        }

        public void Unenroll(int course)
        {
            // Should include logic to handle if the course doesnt exist - omitted for simplicity.
            var courseToUnenroll = Enrollments.First(e => e.CourseId == course);
            Enrollments.Remove(courseToUnenroll);
        }

        // Method to update student information
        public void ChangeProfile(string? newName = null, string? newEmail = null, string? newAddress = null, DateTime? newDateOfBirth = null)
        {
            // Update properties only if new values are provided
            Name = newName ?? Name;
            Email = newEmail ?? Email;
            Address = newAddress ?? Address;
            DateOfBirth = newDateOfBirth ?? DateOfBirth;

            // Additional validation and logic as needed, but omitted for brevity.
        }

        internal void UpdateGrade(int courseId, string letter, decimal numericValue)
        {
            // Omitting any validation or rules for brevity.
            var courseToUpdateGrade = Enrollments.First(e => e.CourseId == courseId);
            courseToUpdateGrade.UpdateGrade(letter, numericValue);
        }
    }

}
