﻿using Noroff.Samples.EFCore.DDDActivity.Domain.StudentAggregate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Noroff.Samples.EFCore.DDDActivity.DataAccess.StudentAccess
{
    public interface IStudentRepository
    {
        Student GetById(int id);
        void Add(Student student);
        void Update(Student student);
        void Delete(Student student);
        void Save();
    }
}
