﻿using Microsoft.EntityFrameworkCore;
using Noroff.Samples.EFCore.DDDActivity.Domain.StudentAggregate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Noroff.Samples.EFCore.DDDActivity.DataAccess.StudentAccess
{
    /// <summary>
    /// Methods like Add, GetById, Update, and SaveChanges handle the basic CRUD operations. These methods encapsulate the EF Core logic, so the domain model doesn't have to deal directly with data persistence concerns.
    /// <para></para>
    /// The repository works with Student aggregate roots, including related entities like Enrollments, ensuring that all data access is consistent with the aggregate boundaries defined in your domain model.
    /// <para></para>
    /// Repositories are primarily concerned with the persistence-related operations - like adding, removing, or retrieving entities — and they usually don't contain business logic. Business logic, especially that which involves coordinating between multiple entities or aggregates, is better placed in domain services or application services (or even command handlers, depending on your architecture.).
    /// <para></para>
    /// Due to EF Core already having implementations of the repository and unit of work pattern, its common to not create your own repositories, but rather use the context directly in the services or handlers you make - although Microsoft still recommend adding this layer for improved testability.
    /// </summary>
    public class StudentRepository : IStudentRepository
    {
        private readonly UniversityContext _context;

        public StudentRepository(UniversityContext context)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
        }

        public Student GetById(int id)
        {
            // Retrieve a student by ID, including any related entities as needed.
            // Recall, our entire aggregate needs to be persisted and returned together.
            // Meaning, whenever we get a student, we get their enrollments, but not the courses - as they are part of a different aggregate.
            return _context.Students
                       .Include(s => s.Enrollments)
                       .SingleOrDefault(s => s.Id == id);
        }

        public void Add(Student student)
        {
            // Add a new student to the context
            _context.Students.Add(student);
        }

        /// <summary>
        /// When using methods like Update, EF Core will automatically check for concurrency conflicts based on the RowVersion property if you've implemented optimistic locking.
        /// </summary>
        /// <param name="student"></param>
        public void Update(Student student)
        {
            // Update an existing student in the context
            // In EF Core, this is typically unnecessary as changes are tracked automatically
            // However, in detached scenarios, you might need to explicitly update the entity state
            _context.Entry(student).State = EntityState.Modified;
        }

        public void Delete(Student student)
        {
            // Remove a student from the context
            _context.Students.Remove(student);
        }

        /// <summary>
        /// This method commits changes to the database. It's a good practice to separate the act of saving changes from the act of adding or updating entities in the repository. This gives you more control over when changes are actually persisted.
        /// </summary>
        public void Save()
        {
            // Save changes to the database
            _context.SaveChanges();
        }
    }
}
