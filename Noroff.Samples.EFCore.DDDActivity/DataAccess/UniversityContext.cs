﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Noroff.Samples.EFCore.DDDActivity.DataAccess
{
    using Microsoft.EntityFrameworkCore;
    using Noroff.Samples.EFCore.DDDActivity.Domain.CourseAggregate;
    using Noroff.Samples.EFCore.DDDActivity.Domain.StudentAggregate;

    public class UniversityContext : DbContext
    {
        public DbSet<Student> Students { get; set; }
        public DbSet<Enrollment> Enrollments { get; set; }
        // <PLACE COURSE AGGREGATE ENTITIES UNDER>
        

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            // Configure your database connection string here
            optionsBuilder.UseSqlServer("Data Source = DESKTOP-HH0G8JM\\SQLEXPRESS; Initial Catalog = UniversityDb; Integrated Security = true; Trust Server Certificate = true;");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // Configure the Student entity
            // The Student entity is configured with a primary key, required properties,
            // and relationships to Enrollments and StudentProfile.
            modelBuilder.Entity<Student>(entity =>
            {
                entity.HasKey(s => s.Id);
                entity.Property(s => s.Name).IsRequired();
                entity.Property(s => s.Email).IsRequired();
                entity.Property(s => s.Address).IsRequired();
                entity.Property(s => s.DateOfBirth).IsRequired();
                // Configure the RowVersion property as a concurrency token
                entity.Property(s => s.RowVersion).IsConcurrencyToken();
                entity.HasMany(s => s.Enrollments).WithOne().HasForeignKey(e => e.StudentId);
            });

            // Configure the Enrollment entity
            // Enrollment includes a primary key and a required CourseId. The Grade value object
            // is configured with the OwnsOne method, specifying how its properties
            // are mapped to the database columns.
            modelBuilder.Entity<Enrollment>(entity =>
            {
                entity.HasKey(e => e.Id);
                entity.Property(e => e.CourseId).IsRequired();
                entity.Property(e => e.StudentId).IsRequired();
                entity.OwnsOne(e => e.Grade, grade =>
                {
                    grade.Property(g => g.Letter).HasColumnName("GradeLetter");
                    grade.Property(g => g.NumericValue).HasColumnName("GradeNumericValue");
                });
            });

            // <PLACE COURSE AGGREGATE CONFIG UNDER>

            
        }
    }

}
