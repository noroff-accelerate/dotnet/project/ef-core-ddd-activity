﻿using Noroff.Samples.EFCore.DDDActivity.DataAccess.StudentAccess;
using Noroff.Samples.EFCore.DDDActivity.DataAccess;
using Noroff.Samples.EFCore.DDDActivity.Domain.StudentAggregate;

namespace Noroff.Samples.EFCore.DDDActivity
{
    internal class Program
    {
        static void Main(string[] args)
        {
            // Initialize the database context
            var context = new UniversityContext();

            // Initialize the repository
            var studentRepository = new StudentRepository(context);

            // Test Data
            string studentName = "John Doe";
            string studentEmail = "john@example.com";
            string studentAddress = "123 Main St";
            DateTime studentDOB = new DateTime(1990, 1, 1);

            // Test: Add a new student
            var newStudent = new Student(studentName, studentEmail, studentAddress, studentDOB);
            studentRepository.Add(newStudent);
            context.SaveChanges();
            Console.WriteLine($"Added new student: {newStudent.Name}");

            //// Test: Retrieve the added student
            //var retrievedStudent = studentRepository.GetById(newStudent.Id);
            //if (retrievedStudent != null)
            //{
            //    Console.WriteLine($"Retrieved student: {retrievedStudent.Name}");

            //    // Test: Enroll the student in a course
            //    int testCourseId = 1; // Assuming this course ID exists
            //    retrievedStudent.Enroll(testCourseId);
            //    context.SaveChanges();
            //    Console.WriteLine($"Enrolled student in course ID {testCourseId}");

            //    // Test: Update grade for an enrollment
            //    var grade = Grade.Create("A", 90);
            //    retrievedStudent.UpdateGrade(testCourseId, grade.Letter, grade.NumericValue);
            //    context.SaveChanges();
            //    Console.WriteLine($"Updated grade to {grade.Letter} for course ID {testCourseId}");

            //    // Verify Enrollments and Grades
            //    foreach (var enrollment in retrievedStudent.Enrollments)
            //    {
            //        Console.WriteLine($"Course ID: {enrollment.CourseId}, Grade: {enrollment.Grade?.Letter}");
            //    }
            //}

            //// Test: Update the student's profile
            //retrievedStudent.ChangeProfile(newName: "John Updated", newEmail: "john_updated@example.com");
            //studentRepository.Update(retrievedStudent);
            //context.SaveChanges();
            //Console.WriteLine("Updated student profile");

            ////// Test: Delete the student
            ////studentRepository.Delete(retrievedStudent);
            ////context.SaveChanges();
            ////Console.WriteLine("Deleted student");

            //// Dispose the DbContext
            //context.Dispose();
        }
    }
}